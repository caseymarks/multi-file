import json
import pickle
from tempfile import NamedTemporaryFile
import unittest

from multi_file import MultiFile


class MultiFileTest(unittest.TestCase):
    def test_pickle(self):
        data = {'a': 1}
        tf = NamedTemporaryFile('w')
        with open(tf.name, 'w+b') as fh:
            with MultiFile(fh, 'w') as mf:
                with mf.open('bar') as fh2:
                    pickle.dump(data, fh2)
        with open(tf.name, 'r+b') as fh:
            with MultiFile(fh, 'r') as mf:
                with mf.open('bar') as fh2:
                    loaded = pickle.load(fh2)
        tf.close()
        self.assertEqual(data, loaded)

    def test_json(self):
        data = {'a': 1}
        tf = NamedTemporaryFile('w')
        with open(tf.name, 'w+b') as fh:
            with MultiFile(fh, 'w') as mf:
                with mf.open('bar', binary=False) as fh2:
                    json.dump(data, fh2)
        with open(tf.name, 'r+b') as fh:
            with MultiFile(fh, 'r') as mf:
                with mf.open('bar', binary=False) as fh2:
                    loaded = json.load(fh2)
        tf.close()
        self.assertEqual(data, loaded)

    def test_nested(self):
        data = {'a': 1}
        tf = NamedTemporaryFile('w')
        with open(tf.name, 'w+b') as fh:
            with MultiFile(fh, 'w') as mf:
                with mf.open('bar') as fh2:
                    with MultiFile(fh2, 'w') as mf2:
                        with mf2.open('baz', binary=False) as fh3:
                            json.dump(data, fh3)
        with open(tf.name, 'r+b') as fh:
            with MultiFile(fh, 'r') as mf:
                with mf.open('bar') as fh2:
                    with MultiFile(fh2, 'r') as mf2:
                        with mf2.open('baz', binary=False) as fh3:
                            loaded = json.load(fh3)
        tf.close()
        self.assertEqual(data, loaded)


if __name__ == '__main__':
    unittest.main()
