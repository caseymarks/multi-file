import setuptools

setuptools.setup(
    name="multi-file",
    version="0.0.1",
    author="Casey Marks",
    author_email="casey@carveacre.com",
    description="put multiple fileobjs inside a fileobj",
    packages=['multi_file'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)
